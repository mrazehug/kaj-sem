# Snake arcade
## Známé chyby
Když jsem aplikaci deploynul na hosting, našel jsem pár chyb, které by mohly 
znepříjemňovat běh projektu. Doporučená nastavení jsou: Firefox, ve kterém je 
potřeba povolit autoplay, aby byla slyšet hudba. Rozlišení 1920x1080. Semestrálka 
jako taková v podstatě je cross browser, nenastane žádná fatální chyba. 
Chybu, kterou absolutně nechápu je ta, že projekt v emulátoru mobilního zařízení 
firefoxu běží bez problémů, v reálném zařízení ale nastanou chyby (třeba tutoriál 
se nezobrazuje úplně v pořádku).

## Popis
Jedná se o snaka, který je ještě rozšířen o minihru (skládání obrázku hada, 
kvůli bodu drag'n'drop). Myšlenka je taková, že projekt je "emulátor arkády",
všechny highscores si proto ukládá do local storage a nepotřebuje připojení 
k internetu.

## Potenciálně matoucí chování
 - Tutoriál se musí přehrát celý až do konce, aby se přestal přehrávat (poslední krok je když hráč najede na minihru), jinak to vypadá, že se tutoriál přehrává donekonečna.

## Jak hrát
 - Pohybuje se šipkami
 - Had když narazí do zdi nebo sám do sebe, tak končí hra
 - Jsou dva typy "potravy" - zelená a fialová, zelená je normální jídlo, fialová spouští minihru
 - Minihra se zobrazuje jen na PC, protože na telefonu by nešla splnit
 - Cílem minihry je srovnat obrázek

## Kde hledat jednotlivé featury
 - Audio (jak jednoduché API, tak pokročilá práce s audiem) ```src/components/organisms/GameManager/SoundManager.ts```
 - Offline aplikace - technicky celá aplikace dokáže běžet offline, stav připojení ovšem nikde nekontroluji.
 - Pokročilé selektory a vendor prefixy - pro stylování používám styled-components, která vendor prefixuje automaticky. Pseudotřídy jsou použity na ```src/components/atoms/Buttons/Button.style.ts```, kde pomocí pseudotříd vytvářím efekt kliknutí jako u tlačítek material ui
 - Media queries - aplikace nepoužívá media queries, ale je responzivní díky flexboxu.
 - Transformace - všechny modaly (například ```src/components/atoms/CenteredModal/CenteredModal.tsx```) jsou vycentrovány transformací. Dále elementy tutoriály jsou pozicovány transformací
 - Aminations, transformations - Již zníněný button má animaci kopírující animaci material ui
 - Pokročilá JS API - používám drag'n'drop a local storage. Drag'n'drop v minihře - ```src/components/molecules/Game/Minigame.tsx```, local storage při ukládání highscores - ```src/utility/HighScoreManager.ts```
 - Ovládání médií - viz bod 1 (tlumení přehrávání nastává při pausnutí hry)

## Puštění lokálně
Pro puštění projektu lokálně stačí pustit následující commandy:
```
git clone git@gitlab.fel.cvut.cz:mrazehug/kaj-sem.git
npm install && npm run start
```
Projekt se spustí na adrese localhost:3000