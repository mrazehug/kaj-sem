export interface Coords {
    x: number,
    y: number
}

export enum Direction {
    UP = "UP",
    DOWN = "DOWN",
    LEFT = "LEFT",
    RIGHT = "RIGHT"
}

export enum FieldType {
    EMPTY="EMPTY",
    SNAKE="SNAKE",
    FOOD="FOOD",
    MINIGAME="MINIGAME",
}

export const MinigameTypes = {
    TILE: "TILE"
}

export enum GameEvents {
    FOOD_EATEN="FOOD_EATEN",
    MINIGAME_STARTED="MINIGAME_STARTED",
    MINIGAME_FINISHED="MINIGAME_FINISHED",
    MINIGAME_SPAWNED="MINIGAME_SPAWNED",
    VICTORY="VICTORY",
    GAME_OVER="GAME_OVER",
    SNAKE_MOVED="SNAKE_MOVED",
    GAME_PAUSED="GAME_PAUSED",
    GAME_RESUMED="GAME_RESUMED",
    GAME_STARTED="GAME_STARTED",
    GAME_ABANDONED="GAME_ABANDONED",
}