const Paths = {
    HOME: "/",
    GAME_SIZE_PICKER: "/game",
    PLAY_GAME: "/game/play",
    LEADERBOARD: "/leaderboard"
}

export default Paths;