import * as ls from "local-storage"

export interface IHighscore {
    name: string,
    highscore: number
}

interface IHighscoreManager {
    saveHighScore: (name: string, highscore: number) => void,
    isHighScore: (highscore: number) => boolean,
    getHighscores: () => Array<IHighscore>
}

const HIGHSCORES_KEY = "highscores";

function HighScoreManager (this: IHighscoreManager) {
    const ensureKey = () => {
        if(ls.get(HIGHSCORES_KEY) === null)
            ls.set(HIGHSCORES_KEY, []);
    }


    this.saveHighScore = (name, highscore) => {
        ensureKey();

        const newHighscores = [ {name, highscore}, ...ls.get(HIGHSCORES_KEY) as Array<IHighscore>];
        ls.set(HIGHSCORES_KEY, newHighscores);
    }

    this.isHighScore = (highscore) => {
        ensureKey();

        const highscores : Array<IHighscore> = ( ls.get(HIGHSCORES_KEY) as Array<IHighscore>);
        return ( highscores.length === 0 || highscore > highscores[0].highscore );
    }

    this.getHighscores = () => {
        ensureKey();

        return ls.get(HIGHSCORES_KEY);
    }
}

export default HighScoreManager;