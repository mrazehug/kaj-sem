import EventBus from "js-event-bus";

export const FOREGROUND_COLOR = "black";
export const BACKGROUND_COLOR = "white";
export const BORDER_COLOR = "grey";

export const Dimensions = {
    d0: "0px",
    d4: "4px",
    d8: "8px",
    d16: "16px",
    d24: "24px",
    d32: "32px",
    d48: "48px",
    d64: "64px",
    d96: "96px",
    d128: "128px"
}

export const eventBus = new EventBus()