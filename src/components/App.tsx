import React from 'react';
import GameManager from "@components/organisms/GameManager/GameManager";
import {BrowserRouter, Route} from "react-router-dom";
import MainMenu from "@components/organisms/MainMenu/MainMenu";
import Paths from "@utility/paths";
import Leaderboard from "@components/organisms/Leaderboard/Leaderboard";

// App sets up routing for the app
const App = () => (
    <BrowserRouter>
        <>
            <Route exact path={Paths.HOME} component={MainMenu}/>
            <Route exact path={Paths.PLAY_GAME} component={GameManager}/>
            <Route exact path={Paths.LEADERBOARD} component={Leaderboard}/>
        </>
    </BrowserRouter>
);

export default App;
