import React from "react";
import Modal from "react-modal";

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

interface ICenteredModalProps {
    isOpen: boolean
}

const CenteredModal : React.FC<ICenteredModalProps> = ({ isOpen, children }) => (
    <Modal isOpen={isOpen} style={customStyles} ariaHideApp={false}>
        {
            children
        }
    </Modal>
)

export default CenteredModal;
