import styled from "styled-components";
import {Dimensions} from "@utility/constants";

interface IStyledInputProps {
    spacing?: string
}

export const StyledInput = styled.input<IStyledInputProps>`
    width: 100%;
    font-family: Pixeboy;
    font-size: ${Dimensions.d24};
    padding: ${Dimensions.d8};
    box-sizing: border-box;
    margin-bottom: ${({spacing}) => spacing ? spacing : "0"};
`