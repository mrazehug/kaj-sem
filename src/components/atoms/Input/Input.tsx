import React from "react";
import {StyledInput} from "@components/atoms/Input/Input.style";
import Label from "@components/atoms/Label/Label";
import {Dimensions} from "@utility/constants";

interface IInputProps {
    onChange?: (e: any) => void,
    label?: string,
    spacing?: string
}

const Input : React.FC<IInputProps> = ({label, spacing, onChange}) => (
    <>
        <Label spacing={Dimensions.d8}>{ label }</Label>
        <StyledInput spacing={spacing} onChange={onChange}/>
    </>
)

export default Input