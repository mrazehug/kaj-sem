import React from "react";
import { H1, H2, H3, H4, H5, H6 } from "@components/atoms/Heading/Heading.style"

const rankTable = {
    1: (children: React.Component, props: any) : any => ( <H1 {...props}>{ children }</H1> ),
    2: (children: React.Component, props: any) : any => ( <H2 {...props}>{ children }</H2> ),
    3: (children: React.Component, props: any) : any => ( <H3 {...props}>{ children }</H3> ),
    4: (children: React.Component, props: any) : any => ( <H4 {...props}>{ children }</H4> ),
    5: (children: React.Component, props: any) : any => ( <H5 {...props}>{ children }</H5> ),
    6: (children: React.Component, props: any) : any => ( <H6 {...props}>{ children }</H6> ),
};

interface IHeadingProps {
    rank: 1|2|3|4|5|6,
    centered?: boolean
}

const Heading: React.FC<IHeadingProps> = ({   rank,
                                              centered,
                                              children,
                                              ...props}) => (
    <>
        {
            // @ts-ignore
            rankTable[rank](children, { centered, ...props})
        }
    </>
)

export default Heading;
