import styled from "styled-components";

interface IHeadingProps {
    centered?: boolean
}

export const H1 = styled.h1<IHeadingProps>`
    font-family: Pixeboy;
    text-align: ${({centered}) => centered ? "center" : "left"};
`;

export const H2 = styled.h2<IHeadingProps>`
    font-family: Pixeboy;
    text-align: ${({centered}) => centered ? "center" : "left"};
`;

export const H3 = styled.h3<IHeadingProps>`
    font-family: Pixeboy;
    text-align: ${({centered}) => centered ? "center" : "left"};
`;

export const H4 = styled.h4<IHeadingProps>`
    font-family: Pixeboy;
    text-align: ${({centered}) => centered ? "center" : "left"};
`;

export const H5 = styled.h5<IHeadingProps>`
    font-family: Pixeboy;
    text-align: ${({centered}) => centered ? "center" : "left"};
`;

export const H6 = styled.h6<IHeadingProps>`
    font-family: Pixeboy;
    text-align: ${({centered}) => centered ? "center" : "left"};
`;