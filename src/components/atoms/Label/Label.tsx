import React from "react";
import {StyledLabel} from "@components/atoms/Label/Label.style";

interface ILabelProps {
        spacing?: string
}

const Label : React.FC<ILabelProps> = ({spacing, children}) => (
    <StyledLabel spacing={spacing}>
            { children }
    </StyledLabel>
)

export default Label