import styled from "styled-components";
import {Dimensions} from "@utility/constants";

interface IStyleLabelProps {
    spacing?: string
}

export const StyledLabel = styled.div<IStyleLabelProps>`
    font-family: Pixeboy;
    margin-bottom: ${({ spacing }) => spacing ? spacing : 0};
    font-size: ${Dimensions.d24}
`