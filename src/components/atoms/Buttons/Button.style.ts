import styled from "styled-components";
import {ButtonSize} from "@components/atoms/Buttons/types";
import {BACKGROUND_COLOR, BORDER_COLOR, Dimensions, FOREGROUND_COLOR} from "@utility/constants";

interface IStyledButtonProps {
    size?: ButtonSize,
    fullWidth?: boolean,
    spacing?: string
}

export const StyledButton = styled.button<IStyledButtonProps>`
    padding: ${({size}) => {
        switch (size){
            case ButtonSize.small:
                return Dimensions.d4;
            case ButtonSize.medium:
                return Dimensions.d8;
            case ButtonSize.large:
                return Dimensions.d16;
            case ButtonSize.xlarge:
                return Dimensions.d32;
            case ButtonSize.xxlarge:
                return Dimensions.d64
            default:
                return Dimensions.d8;
        }
    }};
    color: ${FOREGROUND_COLOR};
    transition: background 0.8s;
    background-position: center;
    background-color: ${BACKGROUND_COLOR};
    border: solid 1px ${BORDER_COLOR};
    width: ${({fullWidth}) => fullWidth ? "100%" : "auto"};
    margin-bottom: ${({spacing}) => spacing ? spacing : "0"};
    cursor: pointer;
    font-family: Pixeboy;
    font-size: ${Dimensions.d24};
    font-size: ${({size}) => {
        switch (size){
            case ButtonSize.small:
                return Dimensions.d16;
            case ButtonSize.medium:
                return Dimensions.d24;
            case ButtonSize.large:
                return Dimensions.d32;
            case ButtonSize.xlarge:
                return Dimensions.d64;
            case ButtonSize.xxlarge:
                return Dimensions.d128
            default:
                return Dimensions.d24;
        }
    }};
    
    &:hover {
       background: #dddddd radial-gradient(circle, transparent 1%, #dddddd 1%) center/15000%; 
    } 
    
    &:active {    
        background-color: #c5c5c5;
        background-size: 100%;
        transition: background 0s;
    }
`;