import React from "react";
import {ButtonSize} from "@components/atoms/Buttons/types";
import {StyledButton} from "@components/atoms/Buttons/Button.style";

export interface IButtonProps {
    text: string,
    size?: ButtonSize,
    fullWidth?: boolean,
    spacing?: string,
    onClick?: () => void,
    delayed?: boolean
}

const Button : React.FC<IButtonProps> = ({   text,
                                             size,
                                             fullWidth,
                                             spacing,
                                             onClick, delayed, ...props}) => (
    <StyledButton size={size}
                  fullWidth={fullWidth}
                  spacing={spacing}
                  onClick={() => {
                      setTimeout(() => {
                          if(onClick) onClick()
                      }, delayed ? 300 : 0)
                  }} {...props}>
        { text }
    </StyledButton>
);

export default Button;