import React from "react";
import {ButtonSize} from "@components/atoms/Buttons/types";
import {StyledButton} from "@components/atoms/Buttons/Button.style";

interface IIconButtonProps{
    icon: any,
    text?: string,
    size?: ButtonSize,
    fullWidth?: boolean,
    spacing?: string,
    onClick?: () => void,
    delayed?: boolean
}

const Button : React.FC<IIconButtonProps> = ({   icon,
                                                 text,
                                                 size,
                                                 fullWidth,
                                                 spacing,
                                                 onClick, delayed, ...props}) => (
    // Same styling as button, except inside there will be container div for the icon and text
    <StyledButton size={size}
                  fullWidth={fullWidth}
                  spacing={spacing}
                  onClick={() => {
                      setTimeout(() => {
                          if(onClick) onClick()
                      }, delayed ? 300 : 0)
                  }} {...props}>
        { icon }{ text }
    </StyledButton>
)

export default Button;