export enum ButtonSize {
    small, medium, large, xlarge, xxlarge
}