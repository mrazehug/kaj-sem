import styled from "styled-components";
import {Dimensions} from "@utility/constants";
import isMobile from "is-mobile";

export const LeaderboardBackButtonContainer = styled.div`
    padding: ${Dimensions.d32} ${Dimensions.d32} 0 ${Dimensions.d32};
    cursor: pointer;
    box-sizing: border-box;
    min-width: 100vw;
`

export const LeaderboardHeader = styled.h1`
    font-family: Pixeboy;
    font-size: ${isMobile() ? Dimensions.d48 : Dimensions.d64};
    margin: ${Dimensions.d32} ${Dimensions.d32} ${Dimensions.d32} ${Dimensions.d32};
    text-align: ${isMobile() ? "left" : "center"};
`

export const LeaderboardTableContainer = styled.div`
    margin: 0 ${Dimensions.d32} 0 ${Dimensions.d32};
    font-family: Pixeboy;
    font-size: ${Dimensions.d32}
`

export const LeaderboardTableHeader = styled.div`
    font-size: ${isMobile() ? Dimensions.d48 : Dimensions.d64};
`

export const LeaderboardTableRow = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: ${Dimensions.d16}
`

export const LeaderboardTableCell = styled.div`
    
`