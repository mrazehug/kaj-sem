import React, {useEffect, useState} from "react"
import HighScoreManager, {IHighscore} from "@utility/HighScoreManager";
import {ArrowLeft} from "react-bootstrap-icons";
import {
    LeaderboardBackButtonContainer, LeaderboardTableCell,
    LeaderboardHeader, LeaderboardTableRow,
    LeaderboardTableContainer, LeaderboardTableHeader
} from "@components/organisms/Leaderboard/Leaderboard.style";
import {useHistory} from "react-router-dom";
import Paths from "@utility/paths";

const Leaderboard : React.FC = () => {
    // @ts-ignore
    const [highscoreManager] = useState(new HighScoreManager());
    const [highscores, setHighscores] = useState<Array<IHighscore>>([]);
    const history = useHistory();

    useEffect(() => {
        setHighscores(highscoreManager.getHighscores())
    }, [])

    const goBack = () => {
        history.push(Paths.HOME);
    }

    return <div>
        <LeaderboardBackButtonContainer onClick={goBack}>
            <ArrowLeft size={48}/>
        </LeaderboardBackButtonContainer>
        <LeaderboardHeader>Leaderboard</LeaderboardHeader>
        <LeaderboardTableContainer>
            <LeaderboardTableHeader>
                <LeaderboardTableRow>
                    <LeaderboardTableCell>
                        Name
                    </LeaderboardTableCell>
                    <LeaderboardTableCell>
                        Score
                    </LeaderboardTableCell>
                </LeaderboardTableRow>
            </LeaderboardTableHeader>
            {
                highscores.map((highscore) => (
                    <LeaderboardTableRow>
                        <LeaderboardTableCell>
                            { highscore.name }
                        </LeaderboardTableCell>
                        <LeaderboardTableCell>
                            { highscore.highscore }
                        </LeaderboardTableCell>
                    </LeaderboardTableRow>
                ))
            }
        </LeaderboardTableContainer>
    </div>
};

export default Leaderboard;
