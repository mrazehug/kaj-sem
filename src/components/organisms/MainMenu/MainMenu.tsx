import React from "react";
import {Joystick, ListOl} from "react-bootstrap-icons";
import Paths from "@utility/paths";
import Menu from "@components/molecules/Menu/Menu";

const MainMenu = () => (
    <Menu header={"The snake arcade"} items={[
        {
            title: "Play game",
            icon: <Joystick size={128}/>,
            to: {
                pathname: Paths.PLAY_GAME,
                state: {
                    dim: 10
                }
            }
        },
        {
            title: "Leaderboard",
            icon: <ListOl size={128}/>,
            to: {
                pathname: Paths.LEADERBOARD
            }
        }
    ]}/>
);

export default MainMenu