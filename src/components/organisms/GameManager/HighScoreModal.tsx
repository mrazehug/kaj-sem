import React, {useState} from "react"
import GameModal from "@components/organisms/GameManager/GameModal";
import Input from "@components/atoms/Input/Input";
import {Dimensions} from "@utility/constants";

interface IHighScoreModal {
    isOpen: boolean,
    onSave: (name: string) => void,
    onRestart: () => void,
    onQuit: () => void
}

const HighScoreModal : React.FC<IHighScoreModal> = ({ isOpen, onSave, onRestart, onQuit }) => {
    const [ name, setName ] = useState("");
    const [ heading, setHeading ] = useState("New high score!")
    const [ saved, setSaved ] = useState(false)

    const _onSave = () => {
        if(name !== "") {
            onSave(name);
            setSaved(true);
            setHeading(`${name}'s highscore saved`);
        }
    }

    return <GameModal isOpen={isOpen} config={{
        heading: heading,
        buttons: saved ? [
            {
                label: "Restart",
                onClick: onRestart
            },
            {
                label: "Quit",
                onClick: onQuit
            }
        ] : [
            {
                label: "name input",
                onClick: () => {},
                render: () => {
                    return <Input label={"Enter your name"} spacing={Dimensions.d16} onChange={(e) => setName(e.target.value)}/>
                }
            },
            {
                label: "Save highscore",
                onClick: _onSave
            },
            {
                label: "Restart",
                onClick: onRestart
            },
            {
                label: "Quit",
                onClick: onQuit
            }
        ]
    }}/>
}

export default HighScoreModal;