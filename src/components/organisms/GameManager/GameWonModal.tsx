import React from "react";
import GameModal from "@components/organisms/GameManager/GameModal";

interface IGameWonModalProps {
    isOpen: boolean,
    score: number,
    onRestart: () => void,
    onGoToLeaderboardClicked: () => void,
    onQuit: () => void
}

const GameWonModal : React.FC<IGameWonModalProps> = ({ isOpen, score, onRestart, onGoToLeaderboardClicked, onQuit }) => (
    <GameModal isOpen={isOpen} config={{
        heading: `Victory! Final score: ${score}`,
        buttons: [
            {
                label: "Restart",
                onClick: onRestart
            },
            {
                label: "Go to leaderboard",
                onClick: onGoToLeaderboardClicked
            },
            {
                label: "Quit",
                onClick: onQuit
            }
        ]}
    }/>
);

export default GameWonModal;