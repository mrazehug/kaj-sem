import React from "react";
import GameModal from "@components/organisms/GameManager/GameModal";

interface IGameOverModalProps {
    isOpen: boolean,
    onRestart: () => void,
    onQuit: () => void
}

const GameOverModal : React.FC<IGameOverModalProps> = ({ isOpen, onRestart, onQuit }) => (
    <GameModal isOpen={isOpen} config={{
        heading: "Game over :(",
        buttons: [
            {
                label: "Restart",
                onClick: onRestart
            },
            {
                label: "Quit",
                onClick: onQuit
            }
        ]}
    }/>
);

export default GameOverModal;