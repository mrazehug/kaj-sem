import {eventBus} from "@utility/constants";
import {GameEvents} from "@utility/types";
// @ts-ignore
import foodEatenSoundSrc from "../../../resources/sounds/FOOD_EATEN.wav"
// @ts-ignore
import snakeMovedSoundSrc from "../../../resources/sounds/SNAKE_MOVED.wav"
// @ts-ignore
import themeMusicSrc from "../../../resources/sounds/THEME_MUSIC.mp3"
// @ts-ignore
import gameOverSrc from "../../../resources/sounds/GAME_OVER.wav"

interface ISoundManager {
    registerEvents: () => void,
    unregisterEvents: () => void,
    stop: () => void,
    init: () => void
}

function SoundManager(this: ISoundManager){
    const themeMusic = new Audio(themeMusicSrc);
    let snakeMovedSound = new Audio(snakeMovedSoundSrc);
    const foodEatenSound = new Audio(foodEatenSoundSrc);
    const gameOverSound = new Audio(gameOverSrc);

    const audioContext = new AudioContext();
    let themeMusicTrack: any;
    let audioEffect: any = null;

    let initialized: boolean = false;

    const playMoveSound = () => {
        snakeMovedSound.play();
        snakeMovedSound = new Audio(snakeMovedSoundSrc);
    }

    const playFoodEatenSound = () => {
        snakeMovedSound.play();
        foodEatenSound.play();
    }

    const muffleMusic = () => {
        const gainNode = audioContext.createGain();
        gainNode.gain.value = 0.2;
        themeMusicTrack.disconnect();
        themeMusicTrack.connect(gainNode).connect(audioContext.destination);
        audioEffect = gainNode;
    }

    const resetSoundEffect = () => {
        audioEffect.disconnect();
        themeMusicTrack.connect(audioContext.destination)
    }

    const handleGameOver = () => {
        themeMusicTrack.disconnect();
        themeMusic.pause();
        themeMusic.currentTime = 0;
        gameOverSound.play();
    }

    const startMusic = () => {
        themeMusicTrack = audioContext.createMediaElementSource(themeMusic);
        themeMusicTrack.connect(audioContext.destination);
        themeMusic.play()
    }

    const stopMusic = () => {
        themeMusicTrack.disconnect();
        themeMusic.pause();
        themeMusic.currentTime = 0;
        gameOverSound.play();
    }

    const registerEvents = () => {
        console.log("Registering events");
        eventBus.on(GameEvents.SNAKE_MOVED, playMoveSound)
        eventBus.on(GameEvents.FOOD_EATEN, playFoodEatenSound)
        eventBus.on(GameEvents.GAME_PAUSED, muffleMusic)
        eventBus.on(GameEvents.GAME_RESUMED, resetSoundEffect)
        eventBus.on(GameEvents.GAME_OVER, handleGameOver)
        eventBus.on(GameEvents.GAME_STARTED, startMusic)
        eventBus.on(GameEvents.GAME_ABANDONED, stopMusic);
    }

    this.init = () => {
        if(initialized)
            return;

        registerEvents();
        initialized = true;
    }
}

console.log("Creating sound manager");

// @ts-ignore
const soundManager = new SoundManager();

export default soundManager