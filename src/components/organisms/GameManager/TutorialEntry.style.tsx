import styled from "styled-components"
import {Dimensions} from "@utility/constants";

export const TutorialEntryContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 250px
`

export const TutorialEntryTextContainer = styled.div`
    color: white;
    font-family: Pixeboy;
    font-size: ${Dimensions.d32};
    text-align: center;
    padding-bottom: ${Dimensions.d8}
`