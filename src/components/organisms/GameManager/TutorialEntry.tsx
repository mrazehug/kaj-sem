import React from "react";
import Button from "@components/atoms/Buttons/Button";
import {
    TutorialEntryContainer,
    TutorialEntryTextContainer
} from "@components/organisms/GameManager/TutorialEntry.style";

interface ITutorialEntryProps {
    text: string,
    onClick: () => void
}

const TutorialEntry: React.FC<ITutorialEntryProps> = ({ text, onClick }) => (
    <TutorialEntryContainer>
        <TutorialEntryTextContainer>
        { text }
        </TutorialEntryTextContainer>
        <Button text={"Next"} onClick={onClick} delayed/>
    </TutorialEntryContainer>
)

export default TutorialEntry