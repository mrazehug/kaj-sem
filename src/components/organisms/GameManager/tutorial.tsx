import ReactDOM from "react-dom"
import TutorialEntry from "@components/organisms/GameManager/TutorialEntry";
import {eventBus} from "@utility/constants";
import {GameEvents} from "@utility/types";
import { CookieStorage } from 'cookie-storage';
import isMobile from "is-mobile";

const createDarkOverlay = () => {
    const darkOverlay = document.createElement('div');
    darkOverlay.style.height = '100vh';
    darkOverlay.style.width  = '100vw';
    darkOverlay.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
    darkOverlay.style.zIndex = "998";
    darkOverlay.style.position = "absolute";
    darkOverlay.style.top = "0"

    return darkOverlay;
}

const createTutorialSectionNextButton = (text = "next", onClick: ()=>void, top: number, translation: string | undefined = undefined) => {
    const container = document.createElement("div");

    container.style.position = "absolute";
    container.style.left = "50%";
    container.style.right = "50%";
    container.style.top = `${top}px`;
    container.style.left = '50%';
    container.style.right = 'auto';
    container.style.marginRight = '-50%';
    if(!translation) {
        container.style.transform = 'translate(-50%)';
    }
    else
        container.style.transform =  `translate(${translation})`;

    ReactDOM.render(<TutorialEntry text={text} onClick={onClick}/>, container);

    return container;
}

const tutorial = (game: any, tutorialElements: any) => {
    const cookieStorage = new CookieStorage();
    if(cookieStorage.getItem("tutorial"))
        return;

    game.pause();

    const darkOverlay = createDarkOverlay();
    const body = document.getElementsByTagName("body")[0];
    const {scoreElement, gameElement, pauseElement} = tutorialElements;
    body.appendChild(darkOverlay);

    let nextButton : any = undefined;

    const completeTutorial = () => {
        const cookieStorage = new CookieStorage();
        const expires = new Date();
        expires.setDate(expires.getDate() + 365)
        cookieStorage.setItem("tutorial", "true", {
            expires
        });
    }

    const renderMinigameSquareTutorial = () => {
        nextButton = createTutorialSectionNextButton("The purple square is minigame. It gives you bonus points upon completion", () => {
            gameElement.style.zIndex = "";
            gameElement.removeChild(nextButton);
            eventBus.detach(GameEvents.MINIGAME_SPAWNED, renderMinigameSquareTutorial);
            body.removeChild(darkOverlay);
            game.resume();
        }, gameElement.getBoundingClientRect().bottom + 10)

        body.appendChild(darkOverlay);
        game.pause();
        gameElement.style.zIndex = "999";
        gameElement.appendChild(nextButton);

        completeTutorial();
    }

    const renderMinigameTutorial = () => {
        nextButton = createTutorialSectionNextButton("This is minigame. Complete it to get bonus points", () => {
            gameElement.style.zIndex = "";
            gameElement.removeChild(nextButton);
            eventBus.detach(GameEvents.MINIGAME_STARTED, renderMinigameTutorial);
            body.removeChild(darkOverlay);
        }, gameElement.getBoundingClientRect().bottom + 10)

        body.appendChild(darkOverlay);
        game.pause();
        gameElement.style.zIndex = "999";
        gameElement.appendChild(nextButton);
    }

    const renderPauseTutorial = () => {
        nextButton = createTutorialSectionNextButton("This is pause button. Click it to pause the game.", () => {
            pauseElement.style.zIndex = "";
            pauseElement.removeChild(nextButton);

            body.removeChild(darkOverlay);
            game.resume();
        }, pauseElement.getBoundingClientRect().bottom + 10, "-200px")

        pauseElement.style.zIndex = "999";
        pauseElement.appendChild(nextButton);

        if(isMobile()) {
            completeTutorial();
        }
    }

    const renderGameTutorial = () => {
        // TODO: Tutorial: use swipes to move
        nextButton = createTutorialSectionNextButton("This is game board. Use arrows to move", () => {
            gameElement.style.zIndex = "";
            gameElement.removeChild(nextButton);
            renderPauseTutorial();
        }, gameElement.getBoundingClientRect().bottom + 10)

        gameElement.style.zIndex = "999";
        gameElement.appendChild(nextButton);

    }

    const renderScoreTutorial = () => {
        nextButton = createTutorialSectionNextButton("This is score. The higher, the better.", () => {
            scoreElement.style.zIndex = "";
            scoreElement.removeChild(nextButton);
            renderGameTutorial();
        }, scoreElement.getBoundingClientRect().bottom)

        scoreElement.style.zIndex = "999";
        scoreElement.appendChild(nextButton);
    }

    renderScoreTutorial();

    const abortTutorial = () => {
        eventBus.detach(GameEvents.MINIGAME_SPAWNED, renderMinigameSquareTutorial);
        eventBus.detach(GameEvents.MINIGAME_STARTED, renderMinigameTutorial);
        eventBus.detach(GameEvents.GAME_ABANDONED, abortTutorial);
        try {
            body.removeChild(darkOverlay);
        }
        catch(e){}
    }

    eventBus.on(GameEvents.MINIGAME_SPAWNED, renderMinigameSquareTutorial);
    eventBus.on(GameEvents.MINIGAME_STARTED, renderMinigameTutorial);
    eventBus.on(GameEvents.GAME_ABANDONED, abortTutorial);
}

export default tutorial;