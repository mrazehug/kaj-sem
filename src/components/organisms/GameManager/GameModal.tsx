import React from "react";
import CenteredModal from "@components/atoms/CenteredModal/CenteredModal";
import Button from "@components/atoms/Buttons/Button";
import Heading from "@components/atoms/Heading/Heading";
import {Dimensions} from "@utility/constants";
import isMobile from "is-mobile";

interface GameModalConfig {
    heading: string,
    buttons: Array<{
        label: string,
        onClick: () => void,
        render?: () => JSX.Element
    }>
}

interface IGamePausedModalProps {
    isOpen: boolean,
    config: GameModalConfig
}

const GameModal : React.FC<IGamePausedModalProps> = ({ isOpen, config }) => (
    <CenteredModal isOpen={isOpen}>
        <div style={{minWidth: isMobile() ? "200px" : "300px", maxWidth: "100vw", boxSizing: "border-box"}}>
            <Heading rank={2} centered>{ config.heading }</Heading>
            {
                config.buttons.map(( button, idx, buttons ) => (
                    button.render ? button.render() : (
                        <Button key={idx}
                                text={button.label}
                                onClick={button.onClick}
                                spacing={idx < buttons.length - 1 ? Dimensions.d16 : Dimensions.d0}
                                fullWidth
                        />
                    )
                ))
            }
        </div>
    </CenteredModal>
);

export default GameModal;