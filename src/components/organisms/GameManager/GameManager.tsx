import React, {useEffect, useRef, useState} from "react";
import Game from "@components/molecules/Game/Game";
import GamePausedModal from "@components/organisms/GameManager/GamePausedModal";
import GameOverModal from "@components/organisms/GameManager/GameOverModal";
import GameWonModal from "@components/organisms/GameManager/GameWonModal";
import {PauseButtonContainer, ScoreButtonContainer} from "@components/organisms/GameManager/GameManager.style";
import {useHistory, useLocation} from "react-router-dom";
import IconButton from "@components/atoms/Buttons/IconButton";
import {ButtonSize} from "@components/atoms/Buttons/types";
import Button from "@components/atoms/Buttons/Button";
import tutorial from "@components/organisms/GameManager/tutorial";
import {eventBus} from "@utility/constants";
import {GameEvents} from "@utility/types";
import soundManager from "@components/organisms/GameManager/SoundManager";
import HighScoreModal from "@components/organisms/GameManager/HighScoreModal";
import HighScoreManager from "@utility/HighScoreManager";

const SCORE_CONTAINER_ID = 'score-container';
const GAME_MOUNT_POINT_ID = 'game-mount-point';
const PAUSE_BUTTON_CONTAINER_ID = 'pause-button-container-id';

// Wrapper for the game class. This wrapper implements UI, score logging, pausing, etc.
// Also creates sound manager and stuff
const GameManager : React.FC<{boardSize: number}> = () => {
    const history = useHistory();
    const location = useLocation();

    const [boardSize] = useState((location.state as any).dim ? (location.state as any).dim : 10)
    const [score, setScore]  = useState(0);
    const [game, setGame]    = useState<any>();
    const [gamePaused, setGamePaused] = useState<boolean>(false);
    const [gameOver, setGameOver] = useState<boolean>(false);
    const [gameWon, setGameWon] = useState<boolean>(false);
    const [highscoreScored, setHighscoreScored] = useState<boolean>(false);
    const gameMountComponent = useRef<HTMLDivElement>(null);

    const onGameOver = (score: number) => {
        // @ts-ignore
        const highScoreManager = new HighScoreManager()
        console.log("Game over :(");
        if(highScoreManager.isHighScore(score))
            setHighscoreScored(true);
        else
            setGameOver(true);
    }

    const onVictory = (score: number) => {
        // @ts-ignore
        const highScoreManager = new HighScoreManager()
        console.log("You won!");
        if(highScoreManager.isHighScore(score))
            setHighscoreScored(true);
        else
            setGameWon(true);
    }

    const onFoodEaten = (score: number) => {
        setScore(score);
    }

    const onMinigameFinished = (score: number) => {
        setScore(score);
    }

    const detachEvents = () => {
        eventBus.detach(GameEvents.FOOD_EATEN, onFoodEaten);
        eventBus.detach(GameEvents.GAME_OVER, onGameOver);
        eventBus.detach(GameEvents.VICTORY, onVictory);
        eventBus.detach(GameEvents.MINIGAME_FINISHED, onMinigameFinished)
    }

    useEffect(() => {
        console.log(score);
    }, [score])

    useEffect(() => {
        eventBus.on(GameEvents.FOOD_EATEN, onFoodEaten);
        eventBus.on(GameEvents.GAME_OVER, onGameOver);
        eventBus.on(GameEvents.VICTORY, onVictory);
        eventBus.on(GameEvents.MINIGAME_FINISHED, (minigameScore: number) => onMinigameFinished(minigameScore));

        return () => {
            detachEvents();
            eventBus.emit(GameEvents.GAME_ABANDONED)
        }
    }, [])

    const onPauseClicked = () => {
        game.pause();
        eventBus.emit(GameEvents.GAME_PAUSED);
        setGamePaused(true);
    }

    const onResumeClicked = () => {
        setGamePaused(false);
        eventBus.emit(GameEvents.GAME_RESUMED);
        game.resume();
    }

    const onRestartClicked = () => {
        setGameOver(false);
        setHighscoreScored(false);
        setScore(0);
        // @ts-ignore
        setGame(new Game(boardSize, boardSize, gameMountComponent.current, onGameOver, onVictory, onFoodEaten))
    }

    const onGoToLeaderboardClicked = () => {
        history.push("/leaderboard");
    }

    const onQuit = () => {
        history.push("/")
        eventBus.emit(GameEvents.GAME_OVER);
    }

    const onHighscoreSaved = (name: string) => {
        // @ts-ignore
        const highscoreManager = new HighScoreManager();
        highscoreManager.saveHighScore(name, score);
    }

    useEffect(()=>{
        // Note: game needs to be stored into variable because setGame is asynchronous and game for
        // tutorial would be undefined.

        soundManager.init();
        // @ts-ignore
        const game = new Game(boardSize, boardSize, gameMountComponent.current);
        setGame(game)


        // Play tutorial, if tutorial was not played
        tutorial(game, {
            gameElement: document.getElementById(GAME_MOUNT_POINT_ID),
            scoreElement: document.getElementById(SCORE_CONTAINER_ID),
            pauseElement: document.getElementById(PAUSE_BUTTON_CONTAINER_ID)
        });
    }, [])

    return <div style={{display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column", width: "100vw", height: "100vh", maxWidth: "100%", maxHeight: "100%"}}>
        <ScoreButtonContainer id={SCORE_CONTAINER_ID}>
            <Button text={score.toString()} size={ButtonSize.xlarge}/>
        </ScoreButtonContainer>
        <PauseButtonContainer id={PAUSE_BUTTON_CONTAINER_ID}>
            <IconButton icon={"Pause"}
                        size={ButtonSize.large}
                        onClick={onPauseClicked}/>
        </PauseButtonContainer>
        <div id={GAME_MOUNT_POINT_ID} ref={gameMountComponent}/>
        <GamePausedModal isOpen={gamePaused}
                         onResume={onResumeClicked}
                         onQuit={onQuit}/>
        <GameOverModal isOpen={gameOver}
                       onRestart={onRestartClicked}
                       onQuit={onQuit}/>
        <HighScoreModal isOpen={highscoreScored}
                        onSave={onHighscoreSaved}
                        onRestart={onRestartClicked}
                        onQuit={onQuit}/>
        <GameWonModal isOpen={gameWon}
                      score={score}
                      onRestart={onRestartClicked}
                      onGoToLeaderboardClicked={onGoToLeaderboardClicked}
                      onQuit={onQuit}/>
    </div>
}

export default GameManager;