import React from "react";
import GameModal from "@components/organisms/GameManager/GameModal";

interface IGamePausedModalProps {
    isOpen: boolean,
    onResume: () => void,
    onQuit: () => void
}

const GamePausedModal : React.FC<IGamePausedModalProps> = ({ isOpen, onResume, onQuit }) => (
    <GameModal isOpen={isOpen} config={{
        heading: "Game paused",
        buttons: [
            {
                label: "Resume",
                onClick: onResume
            },
            {
                label: "Quit",
                onClick: onQuit
            }
        ]}
    }/>
);

export default GamePausedModal;