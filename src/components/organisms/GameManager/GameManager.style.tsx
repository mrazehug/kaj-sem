import styled from "styled-components";
import {Dimensions} from "@utility/constants";

export const PauseButtonContainer = styled.div`
    position: absolute;
    top: ${Dimensions.d8};
    right: ${Dimensions.d8}
`

export const ScoreButtonContainer = styled.div`
    margin-bottom: ${Dimensions.d32}
`