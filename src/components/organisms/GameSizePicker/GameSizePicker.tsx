import React from "react";
import Menu from "@components/molecules/Menu/Menu";
import {Grid3x3GapFill} from "react-bootstrap-icons";
import Paths from "@utility/paths";
import isMobile from "is-mobile"

const GameSizePicker: React.FC  = () => (
    <Menu header={"Select game size"} back={Paths.HOME} items={[
        {
            title: "Small",
            icon: <Grid3x3GapFill size={32}/>,
            to: {
                pathname: Paths.PLAY_GAME,
                state: {
                    dim: 10
                }
            }
        },
        {
            title: "Medium",
            icon: <Grid3x3GapFill size={64}/>,
            disabled: isMobile() ? "This size is available on PC" : undefined,
            to: {
                pathname: Paths.PLAY_GAME,
                state: {
                    dim: 15
                }
            }
        },
        {
            title: "Large",
            icon: <Grid3x3GapFill size={96}/>,
            disabled: isMobile() ? "This size is available on PC" : undefined,
            to: {
                pathname: Paths.PLAY_GAME,
                state: {
                    dim: 20
                }
            }
        }
    ]}/>
);

export default GameSizePicker