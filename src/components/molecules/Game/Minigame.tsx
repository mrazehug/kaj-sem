import React, {useEffect, useState} from "react";
import {MinigameTypes} from "@utility/types";
import {DndProvider, useDrag, useDrop} from "react-dnd";
import {HTML5Backend} from "react-dnd-html5-backend";
import row1col1 from "../../../resources/images/row-1-col-1.jpg"
import row1col2 from "../../../resources/images/row-1-col-2.jpg"
import row1col3 from "../../../resources/images/row-1-col-3.jpg"
import row2col1 from "../../../resources/images/row-2-col-1.jpg"
import row2col2 from "../../../resources/images/row-2-col-2.jpg"
import row2col3 from "../../../resources/images/row-2-col-3.jpg"
import row3col1 from "../../../resources/images/row-3-col-1.jpg"
import row3col2 from "../../../resources/images/row-3-col-2.jpg"
import row3col3 from "../../../resources/images/row-3-col-3.jpg"
import {Dimensions} from "@utility/constants";

interface IMinigameProps {
    onCompletion: (score: number) => void
}

const tiles = [
    {
        requiredOrder: 0,
        image: row1col1
    },
    {
        requiredOrder: 1,
        image: row1col2
    },
    {
        requiredOrder: 2,
        image: row1col3
    },
    {
        requiredOrder: 3,
        image: row2col1
    },
    {
        requiredOrder: 4,
        image: row2col2
    },
    {
        requiredOrder: 5,
        image: row2col3
    },
    {
        requiredOrder: 6,
        image: row3col1
    },
    {
        requiredOrder: 7,
        image: row3col2
    },
    {
        requiredOrder: 8,
        image: row3col3
    }
]

const DroppableArea : React.FC<{tilePositions: Array<any>, setTilePositions: any, index: number}> = ({
                                                                                                          tilePositions,
                                                                                                          setTilePositions,
                                                                                                          index,
                                                                                                          children
}) => {
    const [{ isOver }, drop] = useDrop(() => ({
        accept: MinigameTypes.TILE,
        drop: (item, monitor) => {
            //@ts-ignore
            const draggedIndex = item.index
            const droppedIndex = index
            const newTilePositions = [...tilePositions];
            const tmp = newTilePositions[draggedIndex]
            newTilePositions[draggedIndex] = newTilePositions[droppedIndex]
            newTilePositions[droppedIndex] = tmp
            setTilePositions([...newTilePositions])
        },
        collect: monitor => ({
            isOver: !!monitor.isOver(),
        }),
    }), [tilePositions])

    return <div style={{ border: "solid 1px black", backgroundColor: isOver ? "blue" : "white" }} ref={drop}>{ children }</div>
}


const Draggable : React.FC<{value: any, index: number, image: string}> = ({ value, index, image }) => {
    const [{ isDragging }, drag] = useDrag({
        type: MinigameTypes.TILE,
        item: { value, index },
        collect: (monitor) => ({
            item: monitor.getItem(),
            isDragging: monitor.isDragging()
        }),
    });

    return (
        <div
            ref={drag}
            style={{
                opacity: isDragging ? 0.5 : 1,
                fontWeight: 'bold',
                cursor: 'move',
                height: "100%",
                width: "100%",
                backgroundImage: `url(${image})`,
                backgroundPosition: 'center center',
                backgroundSize: "contain",
                fontFamily: "Pixeboy",
                fontSize: Dimensions.d32
            }}
        >
            { value }
        </div>
    )
}

const shuffle = (arr: Array<any>) => {
    return arr
        .map((a) => ({sort: Math.random(), value: a}))
        .sort((a, b) => a.sort - b.sort)
        .map((a) => a.value)
}

const Minigame : React.FC<IMinigameProps> = ({ onCompletion }) => {
    const [tilePositions, setTilePositions] = useState(shuffle(tiles))

    useEffect(() => {
        let ordered = true
        for(let i = 0; i < tilePositions.length; i++)
        {
            if(i !== 0)
            {
                if(tilePositions[i].requiredOrder !== tilePositions[i - 1].requiredOrder + 1)
                    ordered = false
            }
        }

        if(ordered) {
            onCompletion(Math.floor(Math.random() * 10));
        }
    }, [tilePositions])

    return <>
        <DndProvider backend={HTML5Backend}>
            <div style={{display: "grid", gridTemplateColumns: "repeat(3, 80px)", gridTemplateRows: "repeat(3, 80px)"}}>
                {
                    tilePositions.map((tilePosition, idx) => (
                        <DroppableArea key={idx} tilePositions={tilePositions} setTilePositions={setTilePositions} index={idx}>
                            <Draggable value={tilePosition.requiredOrder} index={idx} image={tilePosition.image}/>
                        </DroppableArea>
                    ))
                }
            </div>
        </DndProvider>
    </>;
}

export default Minigame