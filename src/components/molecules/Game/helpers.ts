import {Coords, FieldType} from "@utility/types";

// Creates FieldType 2D array.
export const createBoard = (    rows: number,
                                cols: number,
                                snake: Array<Coords> = [],
                                food: Coords | undefined = undefined,
                                minigame: Coords | undefined = undefined
) : Array<Array<FieldType>> => {
    let board : Array<Array<FieldType>> = [];
    for(let i = 0; i < rows; ++i) {
        board.push([]);
        for(let j = 0; j < cols; ++j) {
            board[i].push(FieldType.EMPTY);
        }
    }

    snake.forEach((snakePart: Coords) => {
        board[snakePart.x][snakePart.y] = FieldType.SNAKE;
    })

    if(food) board[food.x][food.y] = FieldType.FOOD;
    if(minigame) board[minigame.x][minigame.y] = FieldType.MINIGAME;

    return board;
}

// Returns random board coords.
export const randomCoords = (rows: number, cols: number, snake: Array<Coords> = [], minigame: Coords | undefined = undefined) : Coords => {
    let coords : Coords;

    const generate = () => {
        return ({
            x: Math.floor(Math.random() * cols),
            y: Math.floor(Math.random() * rows)
        })
    }

    const isFoodOnValidPosition = () => {
        const sameCoord = snake.filter((snakePart) => snakePart.x === coords.x && snakePart.y === coords.y);
        console.log("Validating position");
        return sameCoord.length === 0 && !( minigame && ( coords.x === minigame.x && coords.y === minigame.y ));
    }

    do
        coords = generate();
    while(!isFoodOnValidPosition());

    return coords;
}


export const getIndexFromCoords = (tiles: Array<any>, coords: Coords) => {
    let index = null;
    tiles.forEach((tile, idx) => {
        //@ts-ignore
        if(tile.x === coords.x && tile.y === coords.y) index = idx
    })


    return index
}