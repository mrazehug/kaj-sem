import React from "react";
import {FieldType} from "@utility/types";
import styled from "styled-components";

interface IGameBoardProps {
    gameBoard: Array<Array<FieldType>>
}

interface IBoardFieldProps {
    type: FieldType
}

const BoardField = styled.div<IBoardFieldProps>`
    height: 20px;
    width: 20px;
    border: solid 1px black;
    background-color: ${({type}) => {
    switch (type) {
        case FieldType.FOOD:
            return "green";
        case FieldType.SNAKE:
            return "black"
        case FieldType.MINIGAME:
            return "violet"
        default:
            return "white"
        
    }
}}`;

const RowContainer = styled.div`
    display: flex;
`;

// Component for rendering the game board.
const GameBoard : React.FC<IGameBoardProps> = ({gameBoard}) => (
    <>
        {
            gameBoard.map((row, idx) => (
                <RowContainer key={idx}>
                    {
                        row.map((col, jdx) => <BoardField type={col} key={`${idx}_${jdx}`}/>)
                    }
                </RowContainer>
            ))
        }
    </>
);

export default GameBoard;
