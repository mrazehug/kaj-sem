import ReactDOM from "react-dom"
import {Coords, Direction, FieldType, GameEvents} from "@utility/types";
import {createBoard, randomCoords} from "@components/molecules/Game/helpers";
import GameBoard from "@components/molecules/Game/GameBoard";
import Minigame from "@components/molecules/Game/Minigame";
import {Dimensions, eventBus} from "@utility/constants";
import isMobile from "is-mobile";
const SwipeListener = require("swipe-listener");

// timing interval constants - the lower the quicker
const INITIAL_SPEED = 500;
const MAX_SPEED = 250;
const SPEED_DECREMENT = 50;

const KEY_LEFT = 37;
const KEY_UP = 38;
const KEY_RIGHT = 39;
const KEY_DOWN = 40;

// The main challenge of this work. Originally I made an effort to implement it in React,
// however I was not able to make it work as intended, so I switched to vanilla JS.
// You can see the remnants of the React approach in the onGameOver, onVictory, onFoodEaten callbacks.
// I have chosen to keep it this way so it stays somewhat consistent with the rest of this app.
//
// Game component receives following arguments:
// rows: The number of rows of the game board.
// cols: The number of columns of the game board.
// mountingElement: The DOM element into which the game is supposed to render.
// onGameOver: Function that gets called when the game is over.
// onVictory: Function that gets called when player wins (win occurs when length of the snake is equal to area of game board).
// onFoodEaten: Function that gets called whenever snake eats food. This might come in handy when displaying score.
function Game ( rows = 100,
                cols = 100,
                mountingElement: any
) {
    // OCD mode on
    let snake             : Array<Coords>           = [{ x: 0, y: 0 }];
    let direction         : Direction               = Direction.RIGHT;
    let food              : Coords                  = randomCoords(rows, cols, snake);
    let minigame          : Coords | undefined      = undefined;
    let board             : Array<Array<FieldType>> = createBoard(rows, cols, snake, food);
    let gameLoopInterval  : any                     = undefined;
    let speed             : number                  = INITIAL_SPEED;
    const snakeInitialLen : number                  = 1;
    let minigameScore     : number                  = 0;

    // User input handling
    const handleKeyPress = (e: KeyboardEvent) => {
        const { keyCode } = e;
        switch (keyCode) {
            case KEY_LEFT: {
                if(direction !== Direction.RIGHT)
                    direction = Direction.LEFT;
                break;
            }
            case KEY_UP: {
                if(direction !== Direction.DOWN)
                    direction = Direction.UP;
                break;
            }
            case KEY_RIGHT: {
                if(direction !== Direction.LEFT)
                    direction = Direction.RIGHT;
                break;
            }
            case KEY_DOWN: {
                if(direction !== Direction.UP)
                    direction = Direction.DOWN;
                break;
            }
        }
    }

    const handleSwipe = (e: any) => {
        const swipeDirections = e.detail.directions;

        // Note: The library I am using for swiping has not optimal implementation
        // and this part of the code cannot be switch.
        if(swipeDirections.left   && direction !== Direction.RIGHT) direction = Direction.LEFT;
        if(swipeDirections.top    && direction !== Direction.DOWN)  direction = Direction.UP;
        if(swipeDirections.right  && direction !== Direction.LEFT)  direction = Direction.RIGHT;
        if(swipeDirections.bottom && direction !== Direction.UP)    direction = Direction.DOWN;

    }

    const playMinigame = () => {
        stopGame();
        eventBus.emit(GameEvents.MINIGAME_STARTED);

        const handleCompletion = (score: number) => {
            render();
            minigameScore = minigameScore + score
            eventBus.emit(GameEvents.MINIGAME_FINISHED, null, snake.length - snakeInitialLen + minigameScore);
            resumeGame();
        }

        ReactDOM.render(<Minigame onCompletion={handleCompletion}/>, mountingElement)
    }


    // Render func. (Duh)
    const render = () => {
        try {
            // Eventually decided to go with React for rendering. I was a little bit surprised
            // that it works as intended.
            ReactDOM.render(<GameBoard gameBoard={board}/>, mountingElement);
        }
        catch(e) {
            console.error("Following occurred while rendering the Game");
            console.error(e.message);
        }
    }

    const stopGame = () => {
        if(gameLoopInterval) {
            clearInterval(gameLoopInterval);
            gameLoopInterval = null;
        }
    }

    const resumeGame = () => {
        const countDownContainer = document.createElement("div");
        const root = document.getElementById("root");

        countDownContainer.style.position = "fixed";
        countDownContainer.style.height = "100vh";
        countDownContainer.style.width = "100vw";
        countDownContainer.style.display = "flex";
        countDownContainer.style.justifyContent = "center";
        countDownContainer.style.alignItems = "center";
        countDownContainer.style.top = "0";
        countDownContainer.style.textAlign = "center";
        countDownContainer.style.fontFamily = "Pixeboy";
        countDownContainer.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
        countDownContainer.style.color = "white"
        countDownContainer.style.fontSize = Dimensions.d32

        countDownContainer.innerText = "3";

        const one = setTimeout(() => {
            countDownContainer.innerText = "2"
        }, 1000)

        const two = setTimeout(() => {
            countDownContainer.innerText = "1"
        }, 2000)

        const three = setTimeout(() => {
            countDownContainer.innerText = "GO!"
        }, 3000)

        const four = setTimeout(() => {
            if(root)
                root.removeChild(countDownContainer);

            if(!gameLoopInterval)
                gameLoopInterval = setInterval(doStuff, speed);
        }, 4000)

        eventBus.on(GameEvents.GAME_ABANDONED, () => {
            clearInterval(one);
            clearInterval(two);
            clearInterval(three);
            clearInterval(four);
            try {
                if(root)
                    root.removeChild(countDownContainer)
            }
            catch (e){}
        })

        if(root)
            root.appendChild(countDownContainer);
    }

    const crashedSelf = (snake: Array<Coords>, movedSnakeHead: Coords) => {
        const collision = snake.filter((s) => ( s.x === movedSnakeHead.x && s.y === movedSnakeHead.y ));
        return collision.length !== 0;
    }

    // Main game loop of the game.
    const doStuff = () => {
        const movedSnake : Array<Coords> = snake;
        let shouldDisplayMinigame = false;
        let shouldEmitMinigameSpawned = false;

        // First new head of the snake is found based on the current direction
        // The new head is attached to the snake.
        let newSnakeHead : Coords, oldSnakeHead : Coords = snake[0];
        switch (direction) {
            case Direction.RIGHT:
                newSnakeHead = {x: oldSnakeHead.x, y: oldSnakeHead.y + 1};
                break;
            case Direction.LEFT:
                newSnakeHead = {x: oldSnakeHead.x, y: oldSnakeHead.y - 1};
                break;
            case Direction.DOWN:
                newSnakeHead = {x: oldSnakeHead.x + 1, y: oldSnakeHead.y};
                break;
            case Direction.UP:
                newSnakeHead = {x: oldSnakeHead.x - 1, y: oldSnakeHead.y};
                break;
        }

        // If snakes head is out of bounds, its game over
        if(  newSnakeHead.x === rows || newSnakeHead.y === cols
            || newSnakeHead.x === -1 || newSnakeHead.y === -1 || crashedSelf(snake, newSnakeHead))
        {
            eventBus.emit(GameEvents.GAME_OVER, null, movedSnake.length - snakeInitialLen - 1 + minigameScore);
            stopGame();
        }

        movedSnake.unshift(newSnakeHead);

        // If the food coord is hit, new food is placed.
        // If the food is not hit, last piece of the snake is removed
        // otherwise it would grow infinitely without eating food.
        if(movedSnake[0].x === food.x && movedSnake[0].y === food.y) {
            if (speed > MAX_SPEED)
            {
                speed -= SPEED_DECREMENT;
                clearInterval(gameLoopInterval);
                gameLoopInterval = setInterval(doStuff, speed);
            }
            eventBus.emit(GameEvents.FOOD_EATEN, null, movedSnake.length - snakeInitialLen + minigameScore);
            // Check if user haven't won yet.
            if(movedSnake.length === cols * rows)
            {
                eventBus.emit(GameEvents.VICTORY, null, movedSnake.length - snakeInitialLen + minigameScore)
                stopGame();
            }
            else
            {
                food = randomCoords(rows, cols, snake, minigame);
                if(movedSnake.length % 2 === 0 && !isMobile())
                {
                    minigame = randomCoords(rows, cols, snake, minigame);
                    shouldEmitMinigameSpawned = true;
                }
            }
        }
        else {
            eventBus.emit(GameEvents.SNAKE_MOVED);
            movedSnake.pop();
        }


        if(minigame !== undefined && (movedSnake[0].x === minigame.x && movedSnake[0].y === minigame.y)) {
            shouldDisplayMinigame = true;
        }

        // Assigning updated stuff, rerendering the board
        snake = movedSnake;
        if(gameLoopInterval)
        {
            if(shouldDisplayMinigame)
                minigame = undefined

            board = createBoard(rows, cols, snake, food, minigame);
            render();

            if(shouldEmitMinigameSpawned)
                eventBus.emit(GameEvents.MINIGAME_SPAWNED)

            if(shouldDisplayMinigame)
                playMinigame();
        }
    }

    // Initial render of the board, so even the first frame is visible, without this
    // weird bugs happened and I was not able to solve it cleaner.
    render();
    // This needs to be down here because I did not find a way to forward declare in js.
    gameLoopInterval = setInterval(doStuff, INITIAL_SPEED);

    const addInputListener = () => {
        if(isMobile())
        {
            console.log("Registering mobile listener")
            const container = document.getElementById("root");
            if(container) {
                SwipeListener(container);
                container.addEventListener("swipe", handleSwipe)
            }
        }
        else
            document.addEventListener("keydown", handleKeyPress)
    }

    addInputListener();
    eventBus.emit(GameEvents.GAME_STARTED);

    // @ts-ignore
    this.pause = () => {
        stopGame();
    }

    // @ts-ignore
    this.resume = () => {
        resumeGame();
    }
}

export default Game