import React from "react";
import {useHistory} from "react-router-dom";
import {
    MenuHeader,
    MenuCardsContainer,
    MenuContainer,
    MenuCard,
    MenuCardText, MenuCardIconContainer, MenuBackButtonContainer, MenuContentContainer, DisabledOverlay
} from "@components/molecules/Menu/Menu.style";
import {ArrowLeft} from "react-bootstrap-icons";

interface IMenuProps {
    header: string,
    back?: string,
    items: Array<IMenuItem>
}

interface IMenuItem {
    icon?: any,
    title: string,
    disabled?: string,
    to: {
       pathname: string,
       state?: object
    }
}

const Menu : React.FC<IMenuProps> = ({ header, back, items }) => {
    const history = useHistory();

    return <>
        <MenuContainer>
            {
                back && (
                    <MenuBackButtonContainer onClick={() => history.push(back)}>
                        <ArrowLeft size={48}/>
                    </MenuBackButtonContainer>
                )
            }
            <MenuContentContainer>
            <MenuHeader>{ header }</MenuHeader>
            <MenuCardsContainer>
                {
                    items.map((item, idx) => (
                        <MenuCard key={idx} onClick={() => { if(!item.disabled) history.push(item.to) }}>
                            {
                                item.disabled && <DisabledOverlay>{ item.disabled }</DisabledOverlay>
                            }
                            <MenuCardIconContainer>
                                { item.icon }
                            </MenuCardIconContainer>
                            <MenuCardText>{ item.title }</MenuCardText>
                        </MenuCard>
                    ))
                }
            </MenuCardsContainer>
            </MenuContentContainer>
        </MenuContainer>
    </>
}

export default Menu;