import styled from "styled-components"
import {BORDER_COLOR, Dimensions} from "@utility/constants";

export const MenuContainer = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 100vh;
    min-width: 100vw; 
    align-items: center;
`

export const MenuContentContainer = styled.div`
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    max-width: 100vw
`

export const MenuHeader = styled.h1`
    font-family: Pixeboy;
    font-size: ${Dimensions.d64};
    margin: ${Dimensions.d32} ${Dimensions.d32} ${Dimensions.d32} ${Dimensions.d32}
`

export const MenuCardsContainer = styled.div`
    display: flex;
    min-width: 100vw;
    flex-wrap: wrap;
`

export const MenuCard = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    flex-direction: column;
    font-family: Pixeboy;
    font-size: ${Dimensions.d32};
    margin: 0 ${Dimensions.d32} ${Dimensions.d32} ${Dimensions.d32};
    border: solid 1px ${BORDER_COLOR};
    padding: ${Dimensions.d32};
    border-radius: ${Dimensions.d8};
    flex-grow: 1;
    flex-basis: 150px;
    cursor: pointer;
`

export const MenuCardIconContainer = styled.div`
    min-height: ${Dimensions.d128};
    display: flex;
    justify-content: center;
    align-items: center
`

export const MenuCardText = styled.div`
    margin-top: ${Dimensions.d16}
`

export const MenuBackButtonContainer = styled.div`
    padding: ${Dimensions.d32} ${Dimensions.d32} 0 ${Dimensions.d32};
    cursor: pointer;
    box-sizing: border-box;
    min-width: 100vw;
`

export const DisabledOverlay = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    color: white;
    background-color: rgba(0, 0, 0, 0.9)
`